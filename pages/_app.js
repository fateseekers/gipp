import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import 'react-toastify/dist/ReactToastify.min.css';
import '../src/style.css'

import App from 'next/app'
import withReduxStore from '../lib/with-redux-store'
import { Provider } from 'react-redux'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import Loader from "../src/components/Loader";
import React from "react";

class MyApp extends App {
    constructor(props) {
        super(props)
        this.persistor = persistStore(props.reduxStore)
    }

    render() {
        const { Component, pageProps, reduxStore } = this.props
        return (
            <Provider store={reduxStore}>
                <PersistGate
                    loading={<Loader />}
                    persistor={this.persistor}
                >
                    <Component {...pageProps} />
                </PersistGate>
            </Provider>
        )
    }
}

export default withReduxStore(MyApp)