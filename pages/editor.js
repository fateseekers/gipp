import React, {Component} from 'react';
import Layout from "../src/components/Layout";
import dynamic from "next/dynamic";
import fetch from 'isomorphic-unfetch';
import {fetchWithAuth} from "../src/functions/fetchWithAuth";
import {connect} from "react-redux";
import {errorToast, successToast} from "../src/functions/toasts";
import {errorHandle} from "../src/functions/errors";

const Wrapper = dynamic(() => import('../src/components/Wrapper'), {ssr: false})

const EditorWidget = dynamic(import ('../src/editor/EditorConfig'),{ssr:false});

class Editor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            status: this.props.post ? this.props.post[0].status : "main",
            content: [],
            age: this.props.post ? this.props.post[0].age : "",
            signature: this.props.post ? this.props.post[0].signature : "",
        }

        this.onChange = this.onChange.bind(this);
        this.saveContent = this.saveContent.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props !== nextProps;
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    saveContent(data){
        data.blocks.find((el) => {
            if(el.type === "header" && el.data.level === 2){
                this.setState({title: el.data.text})
                return
            }
            if(el.type === "image"){
                this.setState({image: true})
                return
            }
        })
        if((this.state.age || this.state.signature) === undefined) {
            errorToast("Заполните поля до редактора")
            return
        } else if(!this.state.title){
            errorToast("Добавьте заголовок второго уровня!")
            return
        } else if(!this.state.image) {
            errorToast("Добавьте изображение!")
            return
        } else {
            let newObj = data.blocks.map((block) => {
                switch (block.type) {
                    case "header":
                        block.data.text = block.data.text.replace(/"([^"]+)"/g, '«$1»')
                        return block
                    case "paragraph":
                        block.data.text = block.data.text.replace(/"([^"]+)"/g, '«$1»')
                        return block
                    case "image":
                        block.data.text = block.data.caption.replace(/"([^"]+)"/g, '«$1»')
                        return block
                    default:
                        return block
                }
            })
            this.setState({content: newObj})
        }

        if(!this.props.edit) {
            fetchWithAuth(process.env.HOST_API + '/posts',
                {
                    title: this.state.title,
                    age: this.state.age,
                    signature: this.state.signature,
                    status: this.state.status,
                    content: this.state.content
                }, "POST", this.props.token).then(async (res) => {
                let result = await res.json()
                if (res.status > 250) {
                    errorHandle(result.message)
                } else {
                    successToast("Запись успешно добавлена")
                }
            })
        } else {
            fetchWithAuth(process.env.HOST_API + '/posts',
                {
                    id: this.props.post[0].id,
                    title: this.state.title,
                    age: this.state.age,
                    signature: this.state.signature,
                    status: this.state.status,
                    content: this.state.content
                }, "PUT", this.props.token).then(async (res) => {
                let result = await res.json()
                if (res.status > 250) {
                    errorHandle(result.message)
                } else {
                    successToast("Запись успешно обновлена")
                }
            })
        }
    }


    render() {
        return (
            <Wrapper>
                <Layout>
                    <section>
                        <div className="container">
                            <div className="row">
                                <div className="col-12" style={{padding: "0 45px"}}>
                                    {this.props.edit ? <h4>Редактировать новость:</h4> : <h4>Добавить новость:</h4>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <div className="d-flex flex-column">
                                        <form className="d-flex flex-column flex-md-row">
                                            <label className="col-12 col-md-3 d-flex justify-content-between d-md-block align-items-center" htmlFor="age">
                                                <div className="col-4 col-md-12">
                                                    <p className="mb-0 mr-2">Возраст:</p>
                                                </div>
                                                <div className="col-8 col-md-12">
                                                    <input id="age" min={0} max={21} className="form-control" type="number" name="age" placeholder="Укажите возрастное ограничение" defaultValue={this.state.age} onChange={(e) => this.onChange(e)} required={true}/>
                                                </div>
                                            </label>
                                            <label className="col-12 col-md-5 d-flex justify-content-between d-md-block align-items-center" htmlFor="signature">
                                                <div className="col-4 col-md-12">
                                                    <p className="mb-0 mr-2">Подпись:</p>
                                                </div>
                                                <div className="col-8 col-md-12">
                                                    <input id="signature" className="form-control" type="text" name="signature" placeholder="Главный редактор" defaultValue={this.state.signature} onChange={(e) => this.onChange(e)} required={true}/>
                                                </div>
                                            </label>
                                            <label className="col-12 col-md-4 d-flex justify-content-between d-md-block align-items-center" htmlFor="status">
                                                <div className="col-5 col-sm-4 col-md-12">
                                                    <p className="mb-0 mr-2">Тип новости:</p>
                                                </div>
                                                <div className="col-7 col-sm-8 col-md-12">
                                                    <select id="status" className="browser-default custom-select" defaultValue={this.state.status} name="status" onChange={(e) => this.onChange(e)} required={true}>
                                                        <option value="main">Главная новость</option>
                                                        <option value="news">Новость</option>
                                                        <option value="announcement">Анонс</option>
                                                    </select>
                                                </div>
                                            </label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div className="row">
                                <div className="col-12" style={{padding: "0 45px"}}>
                                    <h4>Редактор:</h4>
                                    <EditorWidget save={this.saveContent} data={this.props.post ? this.props.post[0].content : null} btntext={this.props.edit}/>
                                </div>
                            </div>
                        </div>
                    </section>
                </Layout>
            </Wrapper>
        );
    }

    static async getInitialProps ({query}) {
        let result;
        if(query.edit){
            result = await fetch(process.env.HOST_API + '/postbyid?id=' + query.id, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": 'application/json'
                }
            })
        }
        if(query.edit){
            let resultJson = await result.json()
            return {edit: true, post: resultJson.post}
        } else {
            return {edit: false}
        }
    }
}

const mapStateToProps = state => ({
    user: state.user,
    token: state.token
});

export default connect(mapStateToProps, null)(Editor)
