import React, {Component} from 'react'
import Layout from '../src/components/Layout'

import fetch from 'isomorphic-unfetch'
import News from "../src/components/News";
import MainNews from "../src/components/MainNews";
import Router from 'next/router'
import {MDBBtn} from "mdbreact";

class IndexPage extends Component{
    constructor(props) {
        super(props);
    }

    static async getInitialProps() {
        let resPosts = await fetch(process.env.HOST_API + '/posts?page=0', {
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json"
            }
        });
        let posts = await resPosts.json()
        let resMainPosts = await fetch(process.env.HOST_API + '/main_posts?page=0', {
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json"
            }
        });
        let main_posts = await resMainPosts.json()
        return { posts: posts.posts, main_posts: main_posts.posts }
    }

    render() {
        return (
            <Layout title={"Главная страница"} description={"Новостной сайт"} keywords={"Издатели, новости"}>
                <section className="news">
                    <div className="container">
                        <div className="row d-flex flex-column flex-md-row">
                            <div className="col-sm-12 col-lg-8 d-flex flex-column">
                                {this.props.posts && this.props.posts.length > 0
                                    ? <>
                                        <News posts={this.props.posts}/>
                                        <MDBBtn className="px-3 py-2" color={"indigo"} onClick={() => Router.push({pathname: '/news', query: {page: 0}})}>Просмотреть все</MDBBtn>
                                    </>
                                    : null
                                }
                            </div>
                            <div className="col-sm-12 col-lg-4 d-flex flex-column mt-4 mt-lg-0">
                                {this.props.main_posts && this.props.main_posts.length > 0
                                    ? <>
                                        <MainNews posts={this.props.main_posts}/>
                                        <MDBBtn className="px-3 py-2" color={"indigo"} onClick={() => Router.push({pathname: '/news', query: {page: 0, type: "main"}})}>Просмотреть все</MDBBtn>
                                    </>
                                    : null
                                }
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }
}

export default IndexPage
