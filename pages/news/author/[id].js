import React, {Component} from 'react'
import Layout from "../../../src/components/Layout";
import fetch from "isomorphic-unfetch";
import NewsCard from "../../../src/components/NewsCard";
import Pagination from "../../../src/components/Pagination";
import Stub from "../../../src/components/Stub";

class AuthorPosts extends Component{
    render() {
        return (
            <Layout>
                <section>
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                {this.props.posts.length > 0
                                ?  <>
                                        {this.props.posts.map((post) => {
                                             return <NewsCard key={post.id} post={post}/>
                                         })}
                                         <Pagination page={this.props.page} count={this.props.count} url={'/news/author/17?page='}/>
                                 </>
                                : <div>
                                    <Stub text="Издатель не добавил записей"/>
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }

    static async getInitialProps({query}){
        let resPost = await fetch(process.env.HOST_API + `/postbyauthorid`, {
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json"
            },
            method: "POST",
            body: JSON.stringify({
                author_id: query.id,
                page: query.page
            })
        });
        let json = await resPost.json()
        return {posts: json.posts, page: query.page, count: json.count}
    }
}

export default AuthorPosts
