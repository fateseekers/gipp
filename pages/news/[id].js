import React, {Component} from 'react'
import {jsonConverterToHTML} from "../../src/functions/json_converter";
import Layout from "../../src/components/Layout";
import fetch from "isomorphic-unfetch";
import {DateConverter} from "../../src/functions/news";
import Comments from "../../src/components/Comments";
import SocialShare from "../../src/components/SocialShare";
import {MDBBtn, MDBIcon} from "mdbreact";

function CurrentNews ({post, likesCount}) {
    const renderedHtml = jsonConverterToHTML(post[0].content)

    function createMarkup() {
        return {__html: renderedHtml};
    }


    return (
        <Layout title={post[0].title}>
            <section>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="d-flex flex-column">
                                <div dangerouslySetInnerHTML={createMarkup()}></div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-12">
                            <div>
                                <p>Автор: {post[0].signature}</p>
                                <p>Дата: {DateConverter(post[0].date)}</p>
                                <p>Возрастное ограничение: {post[0].age}+ </p>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-12">
                            <div className="d-flex flex-column flex-md-row align-items-center flex-wrap">
                                <p className="mb-2 mb-md-0 mr-2">Поделиться</p>
                                <SocialShare url={window.location.href} title={post[0].title} likes={likesCount} id={post[0].id} like={true}/>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-12">
                            <Comments id={post[0].id}/>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}

CurrentNews.getInitialProps = async ctx => {
    let resPost = await fetch(process.env.HOST_API + `/postbyid?id=${ctx.query.id}`, {
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/json"
        }
    });
    let json = await resPost.json()
    let resLikeCount =  await fetch(process.env.HOST_API + `/likes_count`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/json"
        },
        body: JSON.stringify({id: ctx.query.id})
    });
    let jsonCount = await resLikeCount.json()
    return { post: json.post, likesCount: jsonCount.likes }
}

export default CurrentNews