import React, {Component} from 'react'
import {useRouter} from "next/router";
import fetch from "isomorphic-unfetch";
import Layout from "../../src/components/Layout";
import NewsCard from "../../src/components/NewsCard";
import Link from "next/link";
import Pagination from "../../src/components/Pagination";
import Stub from "../../src/components/Stub";

class NewsOnPages extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Layout>
                <section>
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                {this.props.posts && this.props.posts.length > 0
                                    ?
                                    <>
                                        {this.props.posts.map((post) => {
                                            return <NewsCard key={post.id} post={post}/>
                                        })}
                                        <Pagination url={'/news?page='} count={this.props.count}
                                                    page={this.props.page}/>
                                    </>
                                    : <Stub text="Новости данной рубрики отсутствуют"/>
                                }
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }

    static async getInitialProps({query}){
        let result;
        if(query.type === "main") {
            result = await fetch(process.env.HOST_API + '/main_posts?page=' + (+query.page), {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": 'application/json'
                }
            })
        } else {
            result = await fetch(process.env.HOST_API + '/all_posts?page=' + (+query.page), {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": 'application/json'
                }
            })
        }
        let resultJson = await result.json()
        return {page: query.page, count: resultJson.count, posts: resultJson.posts}
    }
}

export default NewsOnPages
