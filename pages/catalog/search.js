import React, {Component} from 'react'
import Filters from "../../src/components/Filters";
import GoodsCard from "../../src/components/GoodsCard";
import Pagination from "../../src/components/Pagination";
import Layout from "../../src/components/Layout";
import Stub from "../../src/components/Stub";
import fetch from "isomorphic-unfetch";
import Router from "next/router";

class CatalogSearch extends Component{
    constructor(props) {
        super(props);

        this.state = {
            goods: [],
            page: 0,
            age: 0
        }

        this.getFilters = this.getFilters.bind(this)
    }

    static getDerivedStateFromProps(props, prevState){
        if(props.goods!==prevState.goods || props.page!==prevState.page || props.age!==prevState.age){
            return {goods: props.goods, page: props.page, count: props.count, age: props.age, type: props.type};
        }
        else return null;
    }

    getFilters(age, type){
        Router.push({
            pathname: "/catalog/search",
            query: {
                page: 0,
                age: age,
                type: type
            }
        })
        fetch(process.env.HOST_API + '/catalogfilter', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application.json"
            },
            body: JSON.stringify({
                page: this.state.page,
                age: this.state.age,
                type: this.state.type
            })
        }).then(async (res) => {
            let result = await res.json()
            this.setState({goods: result.goods, count: result.count, age: age, type: type})
        })
    }

    render() {
        return (
            <Layout title="Поиск по каталогу | ГИПП">
                <section>
                    <div className="container">
                        <div className="d-flex flex-column flex-md-row">
                            {this.state.goods && this.state.goods.length > 0
                                ?
                                <>
                                    <div className="col-12 col-md-5 col-lg-4 mb-4 mb-md-auto">
                                        <Filters getfilter={this.getFilters}/>
                                    </div>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <h5>Товары:</h5>
                                        {this.state.goods.map((good) => {
                                            return <GoodsCard key={good.id} good={good}/>
                                        })}
                                        <Pagination url={`/catalog/search?age=${this.state.age}&type=${this.state.type}&page=`} count={this.state.count}
                                                    page={this.state.page}/>

                                    </div>
                                </>
                                : <>
                                    <div className="col-12 col-md-5 col-lg-4 mb-4 mb-md-auto">
                                        <Filters getfilter={this.getFilters}/>
                                    </div>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Stub text="Товары не найдены"/>
                                    </div>
                                </>
                            }
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }

    static async getInitialProps({query}){
        let result = await fetch(process.env.HOST_API + '/catalogfilter', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application.json"
            },
            body: JSON.stringify({
                page: query.page,
                age: +query.age,
                type: query.type
            })
        })
        let resultJson = await result.json()
        return {goods: resultJson.goods, page: query.page, count: resultJson.count, age: query.age, type: query.type}
    }
}

export default CatalogSearch
