import React, {Component} from 'react'
import fetch from "isomorphic-unfetch";
import Layout from "../../src/components/Layout";
import {DateConverter, StatusConverter} from "../../src/functions/news";
import {jsonConverterToHTML} from "../../src/functions/json_converter";
import SocialShare from "../../src/components/SocialShare";

function CurrentGood ({good}) {
    const renderedHtml = jsonConverterToHTML(good[0].content)

    function createMarkup() {
        return {__html: renderedHtml};
    }

    return (
        <Layout title={good[0].name + ' | ГИПП'}>
            <section>
                <div className="container">
                    <div className="row">
                        <div className="col-12 d-flex flex-column">
                            <h3>Издание: {good[0].name}</h3>
                            <p>Вид: {StatusConverter(good[0].type)}</p>
                            <p>Дата: {DateConverter(good[0].date)}</p>
                            <p>Возрастное ограничение: {good[0].age}+</p>
                            <h4>Издатель: {good[0].publisher_name}</h4>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-12 d-flex flex-column">
                            <h4>Описание: </h4>
                            <div dangerouslySetInnerHTML={createMarkup()}></div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-12 d-flex align-items-center">
                            <p className="mb-0">Поделиться: </p><SocialShare like={false}/>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}

CurrentGood.getInitialProps = async ({query}) => {
    let resPost = await fetch(process.env.HOST_API + `/catalogbyid?id=` + query.id, {
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/json"
        },
        method: "GET",
    });
    let json = await resPost.json()
    return {good: json.good}
}

export default CurrentGood
