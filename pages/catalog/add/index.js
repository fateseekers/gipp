import React, {Component} from 'react'
import Wrapper from "../../../src/components/Wrapper";
import Layout from "../../../src/components/Layout";
import {MDBInput, MDBPopover, MDBPopoverBody, MDBPopoverHeader, MDBBtn} from "mdbreact";
import dynamic from "next/dynamic";
import {errorToast, successToast} from "../../../src/functions/toasts";
import {fetchWithAuth} from "../../../src/functions/fetchWithAuth";
import {connect} from "react-redux";
import {errorHandle} from "../../../src/functions/errors";

const EditorWidget = dynamic(import ('../../../src/editor/EditorConfig'),{ssr:false});

class CatalogAddItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: this.props.good ? this.props.good[0].name : "",
            contacts: this.props.good ? this.props.good[0].contacts : "",
            type: this.props.good ? this.props.good[0].type : "magazine",
            age: this.props.good ? this.props.good[0].age : "",
            place: this.props.good ? this.props.good[0].place : ""
        }

        this.onChange = this.onChange.bind(this)
        this.saveContent = this.saveContent.bind(this)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props !== nextProps;
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    saveContent(data) {
        let content = data.blocks
        if (typeof (this.state.name) === "") {
            errorToast("Заполните название издания")
            return
        } else if(typeof (this.state.contacts) === ""){
            errorToast("Заполните контакты")
            return
        } else if(typeof (this.state.type) === ""){
            errorToast("Заполните вид издания")
            return
        } else if(typeof (this.state.age) === ""){
            errorToast("Заполните возрастное ограничение")
            return
        } else if(this.state.age < 0 || this.state.age > 17){
            errorToast("Возрастное ограничение некоректно, заполните от 0 до 17")
            return
        } else if(typeof (this.state.place) === ""){
            errorToast("Заполните территорию распространения")
            return
        } else {
            this.setState({content: content})
        }

        if(!this.props.edit) {
            fetchWithAuth(
                process.env.HOST_API + '/catalog',
                this.state,
                "POST",
                this.props.token
            ).then(async (res) => {
                let result = await res.json()
                if (res.status > 250) {
                    errorHandle(result.message)
                } else {
                    successToast("Товар добавлен")
                }
            })
        } else {
            let obj = this.state
                obj.id = this.props.good[0].id
            fetchWithAuth(
                process.env.HOST_API + '/catalog',
                obj,
                "PUT",
                this.props.token
            ).then(async (res) => {
                let result = await res.json()
                if (res.status > 250) {
                    errorHandle(result.message)
                } else {
                    successToast("Товар обновлен")
                }
            })
        }
     }

    render() {
        return (
            <Wrapper>
                <Layout>
                    <section>
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <h4>{this.props.edit ? "Редактировать товар:" : "Добавить товар:"}</h4>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <form className="d-flex flex-column flex-md-row">
                                        <div className="d-flex flex-column mb-2 mb-md-0 col-12 col-md-6">
                                            <MDBInput label="Название издания" name="name" type="text" valueDefault={this.state.name} onChange={(e) => this.onChange(e)}/>
                                            <MDBInput type="textarea" label="Контакты (публичные)" rows="5" name="contacts" valueDefault={this.state.contacts} onChange={(e) => this.onChange(e)}/>
                                            <select className="browser-default custom-select" name="type" valuedefault={this.state.type} onChange={(e) => this.onChange(e)}>
                                                <option disabled>Вид издания:</option>
                                                <option value="magazine">Газета</option>
                                                <option value="journal">Журнал</option>
                                            </select>
                                        </div>
                                        <div className="d-flex flex-column col-12 col-md-6">
                                            <MDBInput label="Возраст. ограничение (0-17)" name="age" type="number" min={0} max={17} valueDefault={this.state.age} onChange={(e) => this.onChange(e)}/>
                                            <MDBInput type="textarea" label="Территория распростр." rows="5" name="place" value={this.state.place} onChange={(e) => this.onChange(e)} />
                                            <MDBPopover
                                                placement="top"
                                                popover
                                                clickable
                                                id="popper1"
                                            >
                                                <MDBBtn color="indigo" className="browser-default m-0 p-2 d-flex justify-content-center">Инструкция</MDBBtn>
                                                <div>
                                                    <MDBPopoverHeader>Инструкция</MDBPopoverHeader>
                                                    <MDBPopoverBody>
                                                        <p>
                                                            Описание должно соответствовать следующим критериям:
                                                        </p>
                                                        <p>
                                                            1. Описание содержит свидетельство о регистрации
                                                            <br/>
                                                            2. Указаны ссылки на сайт/группы (необязательно)
                                                            <br/>
                                                            3. Подписные индексы
                                                            <br/>
                                                            4. Где можно получить онлайн подписку?
                                                            <br/>
                                                            5. Где можно получить товар оффлайн?
                                                            <br/>
                                                            6. Периодичность
                                                        </p>
                                                        <p>В случае не соблюдения правил, модератор вправе отклонить заявку</p>
                                                    </MDBPopoverBody>
                                                </div>
                                            </MDBPopover>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="row mt-4">
                                <div className="col-12">
                                    <EditorWidget save={this.saveContent} placeholder={"Добавьте описание товара"} data={this.props.good ? this.props.good[0].content : null} btntext={this.props.edit}/>
                                </div>
                            </div>
                        </div>
                    </section>
                </Layout>
            </Wrapper>
        );
    }

    static async getInitialProps({query}){
        let result
        if(query.edit){
            result = await fetch(process.env.HOST_API + '/catalogbyid?id=' + query.id, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": 'application/json'
                }
            })
        }
        if(query.edit){
            let resultJson = await result.json()
            return {edit: true, good: resultJson.good}
        } else {
            return {edit: false}
        }
    }
}

const mapStateToProps = state => ({
    token: state.token
});

export default connect(mapStateToProps, null)(CatalogAddItem)
