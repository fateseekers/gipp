import React, {Component} from 'react'
import Layout from "../../src/components/Layout";
import Filters from "../../src/components/Filters";
import Pagination from "../../src/components/Pagination";
import Stub from "../../src/components/Stub";
import GoodsCard from "../../src/components/GoodsCard";
import fetch from 'isomorphic-unfetch'
import Router from 'next/router'

class Catalog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: this.props.page,
            goods: []
        }

        this.getFilters = this.getFilters.bind(this)
    }


    getFilters(age, type){
        Router.push({
            pathname: '/catalog/search',
            query: {
                age: age,
                type: type,
                page: 0
            }
        })
    }

    render() {
        return (
            <Layout title="Каталог | ГИПП">
                <section>
                    <div className="container">
                        <div className="d-flex flex-column flex-md-row">
                            {this.props.goods && this.props.goods.length > 0
                            ?
                                <>
                                    <div className="col-12 col-md-5 col-lg-4 mb-4 mb-md-auto">
                                        <Filters getfilter={this.getFilters}/>
                                    </div>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <h5>Товары:</h5>
                                                {this.props.goods.map((good) => {
                                                    return <GoodsCard key={good.id} good={good}/>
                                                })}
                                                <Pagination url={'/catalog?page='} count={this.props.count}
                                                            page={this.props.page}/>

                                    </div>
                                </>
                                : <Stub text="Товары отсутствуют"/>
                            }
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }

    static async getInitialProps({query}){
        let result = await fetch(process.env.HOST_API + '/catalog?page=' + +query.page, {
            headers: {
                "Content-Type": "application/json",
                "Accept": 'application/json'
            }
        })

        let resultJson = await result.json()
        return {page: query.page, count: resultJson.count, goods: resultJson.goods}
    }
}

export default Catalog
