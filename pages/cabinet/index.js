import React, {Component} from 'react'
import Wrapper from "../../src/components/Wrapper";
import Layout from "../../src/components/Layout";
import {MDBBtn, MDBNav, MDBNavItem, MDBNavLink, MDBTabContent, MDBTabPane} from "mdbreact";
import {connect} from "react-redux";
import NewsCard from "../../src/components/NewsCard";
import Link from "next/link";
import ChangePassword from "../../src/components/ChangePassword";
import ContactUs from "../../src/components/ContactUs";
import ModerateTable from "../../src/components/Moderate";
import GoodsCard from "../../src/components/GoodsCard";

class Cabinet extends Component{
    constructor(props) {
        super(props);

        this.state = {
            activeItem: "1",
            posts: [],
            goods: []
        };
    }

    componentDidMount() {
        fetch(process.env.HOST_API + '/postbyauthorid', {
            method: "POST",
            body: JSON.stringify({author_id: this.props.user.id}),
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.props.token}`
            }
        }).then(async (res) => {
            let result = await res.json()
            this.setState({posts: result.posts})
        })
        fetch(process.env.HOST_API + '/catalogbyauthorid', {
            method: "POST",
            body: JSON.stringify({author_id: this.props.user.id}),
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.props.token}`
            }
        }).then(async (res) => {
            let result = await res.json()
            this.setState({goods: result.goods})
        })
    }


    toggle = tab => e => {
        if (this.state.activeItem !== tab) {
            this.setState({
                activeItem: tab
            });
        }
    };

    render() {
        return (
            <Wrapper>
                <Layout title={"Личный кабинет"}>
                    <section>
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <MDBNav className="nav-tabs d-flex justify-content-between">
                                        <MDBNavItem>
                                            <MDBBtn active={this.state.activeItem === "1"} onClick={this.toggle("1")} role="tab" color="indigo" className="w-100">
                                                Мои записи
                                            </MDBBtn>
                                        </MDBNavItem>
                                        <MDBNavItem>
                                            <MDBBtn active={this.state.activeItem === "2"} onClick={this.toggle("2")} role="tab" color="indigo" className="w-100">
                                                Мои товары
                                            </MDBBtn>
                                        </MDBNavItem>
                                        <MDBNavItem>
                                            <MDBBtn active={this.state.activeItem === "3"} onClick={this.toggle("3")} role="tab" color="indigo" className="w-100">
                                                Смена пароля
                                            </MDBBtn>
                                        </MDBNavItem>
                                        <MDBNavItem>
                                            <MDBBtn color="indigo" waves={"true"} className="w-100">
                                                <Link href="/editor">
                                                    <a className="text-white">Создать новость</a>
                                                </Link>
                                            </MDBBtn>
                                        </MDBNavItem>
                                        <MDBNavItem>
                                            <MDBBtn color="indigo" waves={"true"} className="w-100">
                                                <Link href="/catalog/add">
                                                    <a className="text-white">Создать товар</a>
                                                </Link>
                                            </MDBBtn>
                                        </MDBNavItem>
                                        {this.props.user.role !== "Admin"
                                            ?
                                            <MDBNavItem>
                                                <MDBBtn active={this.state.activeItem === "4"}
                                                        onClick={this.toggle("4")} role="tab" color="indigo"
                                                        className="w-100">
                                                    Обратная связь
                                                </MDBBtn>
                                            </MDBNavItem>
                                            :
                                            <MDBNavItem>
                                                <MDBBtn active={this.state.activeItem === "4"}
                                                        onClick={this.toggle("4")} role="tab" color="indigo"
                                                        className="w-100">
                                                    Модерация
                                                </MDBBtn>
                                            </MDBNavItem>
                                        }
                                    </MDBNav>
                                    <MDBTabContent activeItem={this.state.activeItem} >
                                        <MDBTabPane tabId="1" role="tabpanel">
                                            <div className="mt-2">
                                                <div className="d-flex flex-wrap">
                                                    {this.state.posts.length === 0
                                                        ? <h5 className="mt-2 p-2">Вы еще не создали записей</h5>
                                                        : this.state.posts.map((post) => {
                                                            return (
                                                                <div key={post.id} className="col-12 col-lg-6 p-0 px-sm-4">
                                                                    <NewsCard user={this.props.user} post={post} main={false}/>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        </MDBTabPane>
                                        <MDBTabPane tabId="2" role="tabpanel">
                                            <div className="mt-2">
                                                <div className="d-flex flex-wrap">
                                                    {this.state.goods.length === 0
                                                        ? <h5 className="mt-2 p-2">Вы еще не создали товаров</h5>
                                                        : this.state.goods.map((good) => {
                                                            return (
                                                                <div key={good.id} className="col-12 col-lg-6 p-0 px-sm-4">
                                                                    <GoodsCard user={this.props.user} good={good}/>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        </MDBTabPane>
                                        <MDBTabPane tabId="3" role="tabpanel">
                                            <ChangePassword />
                                        </MDBTabPane>
                                        <MDBTabPane tabId="4" role="tabpanel">
                                            {this.props.user.role !== "Admin"
                                                ? <ContactUs />
                                                : <ModerateTable />
                                            }
                                        </MDBTabPane>
                                    </MDBTabContent>
                                </div>
                            </div>
                        </div>
                    </section>
                </Layout>
            </Wrapper>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
    token: state.token
});

export default connect(mapStateToProps, null)(Cabinet)
