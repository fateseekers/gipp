import React, {Component} from 'react'
import {MDBBtn, MDBInput} from "mdbreact";
import {ToastContainer} from "react-toastify";
import fetch from "isomorphic-unfetch";
import {errorHandle} from "../../../src/functions/errors";
import {errorToast, successToast} from "../../../src/functions/toasts";
import Link from "next/link";
import Router from 'next/router'
import {connect} from "react-redux";

class SignIn extends Component{
    constructor(props) {
        super(props);

        this.inputChange = this.inputChange.bind(this)
        this.authUser = this.authUser.bind(this)
    }

    inputChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    authUser(e){
        e.preventDefault()
        if(this.state.username && this.state.password) {
            let userData = this.state

            fetch(process.env.HOST_API + '/sign_in', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Accept": "application/json"
                },
                body: JSON.stringify(userData)
            }).then(async (res) => {
                let result = await res.json()
                if(res.status > 300){
                    errorHandle(result.message)
                } else {
                    this.props.setToken(result.token)
                    successToast("Вы успешно авторизированы!")
                    setTimeout(() => {
                        Router.push('/')
                    }, 1500)
                }
            })
        } else {
            errorToast("Вы не ввели данные для регистрации")
        }
    }

    render() {
        return (
            <section className="d-flex align-items-center justify-content-center" style={{height: "100vh"}}>
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-6 offset-md-3">
                            <form onSubmit={(e) => this.authUser(e)}>
                                <p className="h5 text-center mb-4">Авторизация</p>
                                <div className="grey-text">
                                    <MDBInput label="Введите логин" icon="user" group type="text" name="username" pattern="[a-zA-Z0-9]+" onChange={(e) => this.inputChange(e)} required={true}/>
                                    <MDBInput label="Введите пароль" icon="lock" group type="password" name="password" onChange={(e) => this.inputChange(e)} required={true}/>
                                </div>
                                <div className="text-center d-flex flex-column">
                                    <MDBBtn color="indigo" type="submit">Авторизация</MDBBtn>
                                    <Link href="/auth/signup">
                                        <a>Еще нет аккаунта? Зарегистрируйтесь</a>
                                    </Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </section>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        setToken: (data) => dispatch({type: "SETTOKEN", payload: data}),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)
