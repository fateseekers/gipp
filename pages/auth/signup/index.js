import React, {Component} from 'react'
import {MDBNav, MDBNavItem, MDBNavLink, MDBTabContent, MDBTabPane, MDBBtn, MDBInput, MDBIcon} from "mdbreact";
import {toast, ToastContainer} from 'react-toastify';
import {errorToast, successToast} from "../../../src/functions/toasts";
import fetch from 'isomorphic-unfetch'
import {errorHandle} from "../../../src/functions/errors";
import Router from 'next/router'
import Link from "next/link";

class SignUp extends Component{
    constructor(props) {
        super(props);

        this.state = {
            activeItem: "1",
            regUser: {
                role: "User"
            },
            regPublisher: {
                role: "Publisher"
            }
        };

        this.regUserFormOnChange = this.regUserFormOnChange.bind(this)
        this.regPublisherFormOnChange = this.regPublisherFormOnChange.bind(this)
        this.regUser = this.regUser.bind(this)
        this.regPublisher = this.regPublisher.bind(this)
    }



    toggle = tab => e => {
        if (this.state.activeItem !== tab) {
            this.setState({
                activeItem: tab
            });
        }
    };

    regUserFormOnChange(e){
        let regUser = this.state.regUser;
        regUser[e.target.name] = e.target.value;
        this.setState({regUser: regUser})
    }

    regPublisherFormOnChange(e){
        let regPublisher = this.state.regPublisher;
        regPublisher[e.target.name] = e.target.value;
        this.setState({regPublisher: regPublisher})
    }

    regUser(e){
        e.preventDefault()
        if(this.state.regUser.username && this.state.regUser.password) {
            let userData = this.state.regUser

            userData['name'] = userData.username

            fetch(process.env.HOST_API + '/sign_up', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Accept": "application/json"
                },
                body: JSON.stringify(userData)
            }).then(async (res) => {
                let result = await res.json()
                if(res.status > 300){
                    errorHandle(result.message)
                } else {
                    successToast("Вы успешно зарегистрированы")
                    setTimeout(() => {
                        Router.push({
                            pathname: '/auth/signin',
                        })
                    }, 1500)
                }
            })
        } else {
            errorToast("Вы не ввели данные для регистрации")
        }
    }

    regPublisher(e){
        e.preventDefault()
        if(this.state.regPublisher.username && this.state.regPublisher.password) {
            let userData = this.state.regPublisher

            fetch(process.env.HOST_API + '/sign_up', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Accept": "application/json"
                },
                body: JSON.stringify(userData)
            }).then(async (res) => {
                let result = await res.json()
                if(res.status > 300){
                    errorHandle(result.message)
                } else {
                    successToast("Вы успешно зарегистрированы")
                    setTimeout(() => {
                        Router.push({
                            pathname: '/auth/signin',
                        })
                    }, 1500)
                }
            })
        } else {
            errorToast("Вы не ввели данные для регистрации")
        }
    }

    render() {
        return (
            <section className="d-flex align-items-center justify-content-center" style={{height: "100vh"}}>
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-8 offset-md-2">
                            <MDBNav className="nav-tabs d-flex">
                                <MDBNavItem className="col-12 col-md-6">
                                    <MDBBtn className="w-100 m-0 mb-1 px-2" color="indigo" active={this.state.activeItem === "1"} onClick={this.toggle("1")} role="tab" >
                                        Пользователь
                                    </MDBBtn>
                                </MDBNavItem>
                                <MDBNavItem className="col-12 col-md-6">
                                    <MDBBtn className="w-100 m-0 mb-1" color="indigo" active={this.state.activeItem === "2"} onClick={this.toggle("2")} role="tab" >
                                        Издатель
                                    </MDBBtn>
                                </MDBNavItem>
                            </MDBNav>
                            <MDBTabContent activeItem={this.state.activeItem} >
                                <MDBTabPane tabId="1" role="tabpanel">
                                    <div className="mt-2">
                                        <form onSubmit={(e) => this.regUser(e)}>
                                            <p className="h5 text-center mb-4">Регистрация</p>
                                            <div className="grey-text">
                                                <MDBInput label="Введите логин (латиница)" icon="user" group type="text" name="username" pattern="[a-zA-Z0-9]+" onChange={(e) => this.regUserFormOnChange(e)} required={true}/>
                                                <MDBInput label="Введите пароль" icon="lock" group type="password" name="password" onChange={(e) => this.regUserFormOnChange(e)} required={true}/>
                                            </div>
                                            <div className="text-center d-flex flex-column">
                                                <MDBBtn color="indigo" type="submit">Зарегистрироваться</MDBBtn>
                                                <Link href="/auth/signin">
                                                    <a>Уже есть аккаунт? Авторизируйтесь</a>
                                                </Link>
                                            </div>
                                        </form>
                                    </div>
                                </MDBTabPane>
                                <MDBTabPane tabId="2" role="tabpanel">
                                    <div className="mt-2">
                                        <form onSubmit={(e) => this.regPublisher(e)}>
                                            <p className="h5 text-center mb-4">Регистрация</p>
                                            <div className="grey-text">
                                                <MDBInput label="Введите логин (латиница)" icon="user" group type="text" name="username" pattern="[a-zA-Z0-9]+" onChange={(e) => this.regPublisherFormOnChange(e)} required={true}/>
                                                <MDBInput label="Введите название издательства" icon="newspaper" name="name" group type="text"  onChange={(e) => this.regPublisherFormOnChange(e)} required={true}/>
                                                <MDBInput label="Введите email" icon="at" name="email" group type="email"  onChange={(e) => this.regPublisherFormOnChange(e)} required={true}/>
                                                <MDBInput label="Описание издательства" icon="align-justify" name="description" group type="text"  onChange={(e) => this.regPublisherFormOnChange(e)} required={true}/>
                                                <MDBInput label="Введите пароль" icon="lock" group type="password" name="password" onChange={(e) => this.regPublisherFormOnChange(e)} required={true}/>
                                            </div>
                                            <div className="text-center d-flex flex-column">
                                                <MDBBtn color="indigo" type="submit">Зарегистрироваться</MDBBtn>
                                                <Link href="/auth/signin">
                                                    <a>Уже есть аккаунт? Авторизируйтесь</a>
                                                </Link>
                                            </div>
                                        </form>
                                    </div>
                                </MDBTabPane>
                            </MDBTabContent>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </section>
        );
    }
}

export default SignUp
