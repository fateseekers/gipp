import React, {Component} from 'react'
import Layout from "../src/components/Layout";
import PublishersList from "../src/components/PublishersList";
import Stub from "../src/components/Stub";

class Publishers extends Component{
    render() {
        return (
            <Layout>
                <section>
                    <div className="container">
                        {this.props.publishers && this.props.publishers.length > 0 ? <>
                                <div className="row">
                                    <div className="col-12">
                                        <h3>Издатели:</h3>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="d-flex flex-wrap">
                                            {this.props.publishers.map((publisher) => {
                                                return <PublishersList key={publisher.id} publisher={publisher}/>
                                            })
                                            }
                                        </div>
                                    </div>
                                </div>
                            </>
                            : <div className="row">
                                <div className="col-12">
                                    <Stub text="Мы ждем издателей"/>
                                </div>
                            </div>
                        }
                    </div>
                </section>
            </Layout>
        );
    }

    static async getInitialProps(){
        let publishers = await fetch(process.env.HOST_API + '/publishers', {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        })

        let publishersJson = await publishers.json()

        return {publishers: publishersJson.publishers}
    }
}

export default Publishers
