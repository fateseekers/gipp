import React from 'react'
import Stub from "../src/components/Stub";

export default function Custom404(){
    return (
        <Stub text={`Неизвестная страница. Ошибка 404. `} err={404}/>
    )
}