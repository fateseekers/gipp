import React, {Component} from 'react'
import fetch from "isomorphic-unfetch";
import PublishersList from "../src/components/PublishersList";
import NewsCard from "../src/components/NewsCard";
import Layout from "../src/components/Layout";
import Pagination from "../src/components/Pagination";
import {MDBIcon} from "mdbreact";

class Search extends Component{
    constructor(props) {
        super(props);

        this.state = {
            posts: false,
            publishers: false
        }
    }


    render() {
        return (
            <Layout>
                <section>
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <h3>Результаты поиска:</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="accordion" id="accordionExample">
                                    {this.props.publishers && this.props.publishers.length > 0
                                        ?
                                        <div className="card z-depth-1">
                                            <div className="card-header" id="headingOne">
                                                <h5 className="mb-0">
                                                    <button className="btn btn-link accordion__button" type="button" data-toggle="collapse"
                                                            data-target="#collapseOne"
                                                            aria-expanded={this.state.publishers} aria-controls="collapseOne" onClick={() => {this.setState({publishers: !this.state.publishers})}}>
                                                        Издатели:
                                                        <MDBIcon size="2x" icon={!this.state.publishers ? `arrow-down` : `arrow-up`}/>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" className={`accordion__collapse${this.state.publishers ? " show" : " collapse"}`} aria-labelledby="headingOne"
                                                 data-parent="#accordionExample">
                                                <div className="card-body">
                                                    {this.props.publishers.map((publisher) => {
                                                        return <PublishersList key={publisher.id} publisher={publisher}/>
                                                    })}
                                                </div>
                                            </div>
                                        </div>
                                        : null
                                    }
                                    {this.props.posts && this.props.posts.length > 0
                                        ?
                                        <div className="card z-depth-1">
                                            <div className="card-header" id="headingOne">
                                                <h5 className="mb-0">
                                                    <button className="btn btn-link accordion__button" type="button" data-toggle="collapse"
                                                            data-target="#collapseOne"
                                                            aria-expanded={this.state.posts} aria-controls="collapseOne" onClick={() => this.setState({posts: !this.state.posts})}>
                                                        Записи:
                                                        <MDBIcon size="2x" icon={!this.state.posts ? `arrow-down` : `arrow-up`}/>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" className={`accordion__collapse${this.state.posts ? " show" : " collapse"}`} aria-labelledby="headingOne"
                                                 data-parent="#accordionExample">
                                                <div className="card-body">
                                                    {this.props.posts.map((post) => {
                                                        return <NewsCard key={post.id} post={post}/>
                                                    })}
                                                    <Pagination count={this.props.count} page={this.props.page} url={`/search?str=${this.props.str}&page=`}/>
                                                </div>
                                            </div>
                                        </div>
                                        : null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        )
    }

    static async getInitialProps({query}){
        let result = await fetch(`${process.env.HOST_API}/search`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify({
                page: query.page,
                str: query.str
            })
        })
        let resultJson = await result.json()
        return {page: query.page, count: resultJson.count, posts: resultJson.posts, publishers: resultJson.publishers, str: query.str}
    }
}

export default Search
