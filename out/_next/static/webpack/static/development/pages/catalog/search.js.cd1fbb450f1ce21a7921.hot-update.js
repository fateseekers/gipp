webpackHotUpdate("static\\development\\pages\\catalog\\search.js",{

/***/ "./pages/catalog/search.js":
/*!*********************************!*\
  !*** ./pages/catalog/search.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _src_components_Filters__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../src/components/Filters */ "./src/components/Filters.js");
/* harmony import */ var _src_components_GoodsCard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../src/components/GoodsCard */ "./src/components/GoodsCard.js");
/* harmony import */ var _src_components_Pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../src/components/Pagination */ "./src/components/Pagination.js");
/* harmony import */ var _src_components_Layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../src/components/Layout */ "./src/components/Layout.js");
/* harmony import */ var _src_components_Stub__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../src/components/Stub */ "./src/components/Stub.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_13__);






var _jsxFileName = "C:\\Users\\Pavel\\Desktop\\gipp\\pages\\catalog\\search.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement;

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function () { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }










var CatalogSearch = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(CatalogSearch, _Component);

  var _super = _createSuper(CatalogSearch);

  function CatalogSearch() {
    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, CatalogSearch);

    return _super.apply(this, arguments);
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(CatalogSearch, [{
    key: "getFilters",
    value: function getFilters(age, type) {
      next_router__WEBPACK_IMPORTED_MODULE_13___default.a.replace({
        pathname: '/catalog/search',
        query: {
          age: age,
          type: type,
          page: 1
        }
      });
      next_router__WEBPACK_IMPORTED_MODULE_13___default.a.reload();
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      return __jsx(_src_components_Layout__WEBPACK_IMPORTED_MODULE_10__["default"], {
        title: "\u041F\u043E\u0438\u0441\u043A \u043F\u043E \u043A\u0430\u0442\u0430\u043B\u043E\u0433\u0443 | \u0413\u0418\u041F\u041F",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 26,
          columnNumber: 13
        }
      }, __jsx("section", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 27,
          columnNumber: 17
        }
      }, __jsx("div", {
        className: "container",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 28,
          columnNumber: 21
        }
      }, __jsx("div", {
        className: "d-flex flex-column flex-md-row",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 29,
          columnNumber: 25
        }
      }, this.props.goods && this.props.goods.length > 0 ? __jsx(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, __jsx("div", {
        className: "col-12 col-md-5 col-lg-4 mb-4 mb-md-auto",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 37
        }
      }, __jsx(_src_components_Filters__WEBPACK_IMPORTED_MODULE_7__["default"], {
        getfilter: this.getFilters,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 41
        }
      })), __jsx("div", {
        className: "col-12 col-md-7 col-lg-8",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 36,
          columnNumber: 37
        }
      }, __jsx("h5", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 41
        }
      }, "\u0422\u043E\u0432\u0430\u0440\u044B:"), this.props.goods.map(function (good) {
        return __jsx(_src_components_GoodsCard__WEBPACK_IMPORTED_MODULE_8__["default"], {
          key: good.id,
          good: good,
          __self: _this,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 52
          }
        });
      }), __jsx(_src_components_Pagination__WEBPACK_IMPORTED_MODULE_9__["default"], {
        url: "/catalog/search?age=".concat(this.props.age, "&type=").concat(this.props.type, "&page="),
        count: this.props.count,
        page: this.props.page,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41,
          columnNumber: 41
        }
      }))) : __jsx(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, __jsx("div", {
        className: "col-12 col-md-5 col-lg-4 mb-4 mb-md-auto",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47,
          columnNumber: 37
        }
      }, __jsx(_src_components_Filters__WEBPACK_IMPORTED_MODULE_7__["default"], {
        getfilter: this.getFilters,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48,
          columnNumber: 41
        }
      })), __jsx("div", {
        className: "col-12 col-md-7 col-lg-8",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50,
          columnNumber: 37
        }
      }, __jsx(_src_components_Stub__WEBPACK_IMPORTED_MODULE_11__["default"], {
        text: "\u0422\u043E\u0432\u0430\u0440\u044B \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u044B",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51,
          columnNumber: 41
        }
      })))))));
    }
  }], [{
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var query, result, resultJson;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function getInitialProps$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              query = _ref.query;
              _context.next = 3;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12___default()("http://localhost:8000" + '/catalogfilter', {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                  "Accept": "application.json"
                },
                body: JSON.stringify({
                  page: query.page - 1,
                  age: query.age,
                  type: query.type
                })
              }));

            case 3:
              result = _context.sent;
              _context.next = 6;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(result.json());

            case 6:
              resultJson = _context.sent;
              return _context.abrupt("return", {
                goods: resultJson.goods,
                page: query.page,
                count: resultJson.count,
                age: query.age,
                type: query.type
              });

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, null, null, null, Promise);
    }
  }]);

  return CatalogSearch;
}(react__WEBPACK_IMPORTED_MODULE_6__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (CatalogSearch);

/***/ })

})
//# sourceMappingURL=search.js.cd1fbb450f1ce21a7921.hot-update.js.map