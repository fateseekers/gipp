webpackHotUpdate("static\\development\\pages\\catalog\\search.js",{

/***/ "./pages/catalog/search.js":
/*!*********************************!*\
  !*** ./pages/catalog/search.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _src_components_Filters__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../src/components/Filters */ "./src/components/Filters.js");
/* harmony import */ var _src_components_GoodsCard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../src/components/GoodsCard */ "./src/components/GoodsCard.js");
/* harmony import */ var _src_components_Pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../src/components/Pagination */ "./src/components/Pagination.js");
/* harmony import */ var _src_components_Layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../src/components/Layout */ "./src/components/Layout.js");
/* harmony import */ var _src_components_Stub__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../src/components/Stub */ "./src/components/Stub.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12__);






var _jsxFileName = "C:\\Users\\Pavel\\Desktop\\gipp\\pages\\catalog\\search.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement;

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function () { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }









var CatalogSearch = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(CatalogSearch, _Component);

  var _super = _createSuper(CatalogSearch);

  function CatalogSearch(props) {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, CatalogSearch);

    _this = _super.call(this, props);
    _this.state = {
      goods: [],
      page: 1,
      age: _this.props.age
    };
    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(CatalogSearch, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      console.log(this.state);
    }
  }, {
    key: "getFilters",
    value: function getFilters(age, type) {
      this.setState({
        age: age,
        type: type
      });
      this.getGoods();
    }
  }, {
    key: "getGoods",
    value: function getGoods() {
      var _this2 = this;

      console.log(this.state);
      isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12___default()("http://localhost:8000" + '/catalogfilter', {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Accept": "application.json"
        },
        body: JSON.stringify({
          page: this.state.page - 1,
          age: this.state.age,
          type: this.state.type
        })
      }).then(function _callee(res) {
        var result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res.json());

              case 2:
                result = _context.sent;
                console.log(result);

                _this2.setState({
                  goods: result.goods,
                  count: result.count,
                  age: age,
                  type: type
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, null, null, null, Promise);
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return __jsx(_src_components_Layout__WEBPACK_IMPORTED_MODULE_10__["default"], {
        title: "\u041F\u043E\u0438\u0441\u043A \u043F\u043E \u043A\u0430\u0442\u0430\u043B\u043E\u0433\u0443 | \u0413\u0418\u041F\u041F",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59,
          columnNumber: 13
        }
      }, __jsx("section", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60,
          columnNumber: 17
        }
      }, __jsx("div", {
        className: "container",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61,
          columnNumber: 21
        }
      }, __jsx("div", {
        className: "d-flex flex-column flex-md-row",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62,
          columnNumber: 25
        }
      }, this.state.goods && this.state.goods.length > 0 ? __jsx(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, __jsx("div", {
        className: "col-12 col-md-5 col-lg-4 mb-4 mb-md-auto",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66,
          columnNumber: 37
        }
      }, __jsx(_src_components_Filters__WEBPACK_IMPORTED_MODULE_7__["default"], {
        getfilter: this.getFilters,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 67,
          columnNumber: 41
        }
      })), __jsx("div", {
        className: "col-12 col-md-7 col-lg-8",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69,
          columnNumber: 37
        }
      }, __jsx("h5", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70,
          columnNumber: 41
        }
      }, "\u0422\u043E\u0432\u0430\u0440\u044B:"), this.state.goods.map(function (good) {
        return __jsx(_src_components_GoodsCard__WEBPACK_IMPORTED_MODULE_8__["default"], {
          key: good.id,
          good: good,
          __self: _this3,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 72,
            columnNumber: 52
          }
        });
      }), __jsx(_src_components_Pagination__WEBPACK_IMPORTED_MODULE_9__["default"], {
        url: "/catalog/search?age=".concat(this.state.age, "&type=").concat(this.state.type, "&page="),
        count: this.state.count,
        page: this.state.page,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74,
          columnNumber: 41
        }
      }))) : __jsx(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, __jsx("div", {
        className: "col-12 col-md-5 col-lg-4 mb-4 mb-md-auto",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80,
          columnNumber: 37
        }
      }, __jsx(_src_components_Filters__WEBPACK_IMPORTED_MODULE_7__["default"], {
        getfilter: this.getFilters,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81,
          columnNumber: 41
        }
      })), __jsx("div", {
        className: "col-12 col-md-7 col-lg-8",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83,
          columnNumber: 37
        }
      }, __jsx(_src_components_Stub__WEBPACK_IMPORTED_MODULE_11__["default"], {
        text: "\u0422\u043E\u0432\u0430\u0440\u044B \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u044B",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84,
          columnNumber: 41
        }
      })))))));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, prevState) {
      if (props.goods !== prevState.goods) {
        return {
          goods: props.goods,
          page: props.page,
          count: props.count,
          age: props.age,
          type: props.type
        };
      } else return null;
    }
  }, {
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var query, result, resultJson;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function getInitialProps$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              query = _ref.query;
              console.log(query);
              _context2.next = 4;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_12___default()("http://localhost:8000" + '/catalogfilter', {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                  "Accept": "application.json"
                },
                body: JSON.stringify({
                  page: +query.page - 1,
                  age: +query.age,
                  type: query.type
                })
              }));

            case 4:
              result = _context2.sent;
              _context2.next = 7;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(result.json());

            case 7:
              resultJson = _context2.sent;
              console.log(resultJson);
              return _context2.abrupt("return", {
                goods: resultJson.goods,
                page: query.page,
                count: resultJson.count,
                age: query.age,
                type: query.type
              });

            case 10:
            case "end":
              return _context2.stop();
          }
        }
      }, null, null, null, Promise);
    }
  }]);

  return CatalogSearch;
}(react__WEBPACK_IMPORTED_MODULE_6__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (CatalogSearch);

/***/ })

})
//# sourceMappingURL=search.js.258c7327b7296c5ea2aa.hot-update.js.map