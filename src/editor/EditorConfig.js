import React from "react";

import EditorJS from "@editorjs/editorjs";
import ImageTool from '@editorjs/image';
import List from '@editorjs/list'
import LinkTool from '@editorjs/link'
import Header from '@editorjs/header'
import Paragraph from '@editorjs/paragraph'
import {MDBBtn} from "mdbreact";
import {connect} from "react-redux";

function EditorConfig({save, data, placeholder, btntext, token}){
    const editor = new EditorJS({
        holder: "editorjs",
        placeholder: placeholder || 'Добавьте контент',
        tools: {
            image: {
                class: ImageTool,
                config: {
                    endpoints: {
                        byFile: process.env.HOST_API + '/file_upload', // Your backend file uploader endpoint
                    },
                    additionalRequestHeaders: {
                        Authorization: `Bearer ${token}`
                    }
                },
            },
            header: {
                class: Header,
                config: {
                    placeholder: 'Добавьте заголовок',
                    levels: [2, 3, 4],
                    defaultLevel: 3
                }
            },
            linkTool: {
                class: LinkTool,
                config: {
                    endpoint: process.env.HOST_API + '/link', // Your backend endpoint for url data fetching
                }
            },
            paragraph: {
                class: Paragraph,
                inlineToolbar: true,
            },
            list: {
                class: List,
                inlineToolbar: true,
            },
        },
        data: {blocks: data !== undefined ? JSON.parse(data) : ""}
    })

    function getData() {
        editor.save().then((outputData) => {
            save(outputData);
        }).catch((error) => {
            console.log('Saving failed: ', error)
        });
    }

    return (
        <>
            <div id="editorjs"></div>
            <MDBBtn color="indigo" onClick={getData}>{btntext ? "Редактировать" : "Создать"}</MDBBtn>
        </>
    )
}


const mapStateToProps = state => ({
    token: state.token
});

export default connect(mapStateToProps, null)(EditorConfig)
