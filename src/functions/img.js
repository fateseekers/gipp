import tinycolor from 'tinycolor2'

export function Color(rgb){
    let colorArr = rgb.split(", "),
        color = tinycolor({r:colorArr[0], g:colorArr[1], b:colorArr[2]})

    return color.isDark()
}