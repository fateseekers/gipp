import fetch from 'isomorphic-unfetch'

export async function fetchWithAuth(url, data, method, token){
     if(method === "GET"){
         return fetch(url, {
             headers: {
                 'Content-Type': 'application/json',
                 'Accept': 'application/json',
                 'Authorization': `Bearer ${token}`
             }
         })
     } else {
         return fetch(url, {
             headers: {
                 'Content-Type': 'application/json',
                 'Accept': 'application/json',
                 'Authorization': `Bearer ${token}`
             },
             body: JSON.stringify(data),
             method: method
         })
     }
}