import React from "react";
import trimCharacters from 'trim-characters';

export function jsonConverterToHTML(html){
    let htmlBlocks = ``,
        jsonHTML = JSON.parse(html)
        for (let block of jsonHTML) {
            switch (block.type) {
                case 'paragraph':
                    htmlBlocks += `<p class="ce-paragraph cdx-block mt-4">${block.data.text.replace(/["«|»"]/g, '"').replace("href", 'target="_blank" href')}</p>`;
                    break;
                case 'header':
                    htmlBlocks += `<h${block.data.level} class="ce-header cdx-block">${block.data.text}</h${block.data.level}>`;
                    break;
                case 'list':
                    htmlBlocks += generateList(block)
                    break;
                case 'image':
                    htmlBlocks += `<img class="img-fluid" src=${block.data.file.url} alt=${block.data.file.caption ? block.data.file.caption : ""}>`
                    break;
                case 'linkTool':
                    htmlBlocks += `
                    <div class="link-tool z-depth-1 p-4">
                        <a class="link-tool__content link-tool__content--rendered" target="_blank" rel="nofollow noindex noreferrer" href=${block.data.link}>
                            <div class="link-tool__title">
                                <b class="black-text">${block.data.meta.title}</b>
                            </div>
                            <span class="link-tool__anchor">${trimCharacters(block.data.link, 30, true , "...")}</span>
                        </a>
                    </div>`
            }
        }
    return htmlBlocks;
}

export function generateList(block){
    if(block.data.style === "unordered"){
        let list = ``;
        list += `<ul class="list-group ml-3">`;
        for (let item of block.data.items) {
            list +=  `<li class="list-item">${item}</li>`
        }
        list += `</ul>`
        return list
    } else {
        let list = ``;
        list += `<ol class="list-group ml-3">`;
        for (let item of block.data.items) {
            list +=  `<li class="list-item">${item}</li>`
        }
        list += `</ol>`
        return list
    }
}