import {errorToast} from "./toasts";

export function errorHandle(type){
    switch (type) {
        case "USER CONSIST":
            errorToast("Логин уже существует");
            break
        case "USER NOT FOUND":
            errorToast("Пользователь не найден");
            break;
        case "PASSWORDS DOESNT MATCH":
            errorToast("Старый пароль неправильный");
            break
        case "PASSWORDS NOT EQUAL":
            errorToast("Пароль неверный");
            break
        case "CONTENT NOT GIVEN":
            errorToast("Данные не были отправлены")
            break
        case "POST NOT FOUND":
            errorToast("Запись не найцдена")
            break
        case "PERMISSIONS DENIED":
            errorToast("Для совершения действия необходимо авторизоваться")
            break
    }
}