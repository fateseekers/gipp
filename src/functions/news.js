/**
 * @return {string}
 */
export function DateConverter(date){
    let data = new Date(date),
        day = data.getDate() < 10 ? "0" + data.getDate() : data.getDate(),
        month = (data.getMonth() + 1) < 10 ? "0" + (data.getMonth() + 1) : data.getMonth() + 1,
        year = data.getFullYear()
    return day + "." + month + "." + year
}

/**
 * @return {string}
 */
export function StatusConverter(status){
    switch (status) {
        case "news":
            return "Новость";
        case "main":
            return "Главная новость";
        case "announcement":
            return "Анонс";
        case "magazine":
            return "Газета"
        case "journal":
            return "Журнал"
        default:
            break
    }
}