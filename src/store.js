import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import jwt from "jsonwebtoken";

const startState = {}

export const reducer = (state = startState, action) => {
    switch (action.type) {
        case "GETUSER":
            return state.user
        case "SETTOKEN":
            let decoded = jwt.decode(action.payload)

            return Object.assign({}, state, {
                user: decoded.user,
                token: action.payload
            })
        case "RESETUSER":
            return state = {}
        default:
            return state
    }
}

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['user', 'token'], // place to select which state you want to persist
}

const persistedReducer = persistReducer(persistConfig, reducer)

export function initializeStore(initialState = startState) {
    return createStore(
        persistedReducer,
        initialState,
        composeWithDevTools(applyMiddleware())
    )
}