import React from 'react'
import {MDBIcon} from "mdbreact";
import {Color} from "../functions/img";

const Tools = ({image, deletePost, updatePost}) => {
    return (
        <div className="newscard__tools d-flex">
            <MDBIcon
                className={`mr-2 ${image ? Color(image) ? "white-text" : "black-text" : "black-text"}`}
                icon="times-circle" size="2x" onClick={(e) => {
                e.preventDefault();
                deletePost()
            }}/>
            <MDBIcon className={`${image ? Color(image) ? "white-text" : "black-text" : "black-text"}`}
                     icon="edit" size="2x" onClick={(e) => {
                e.preventDefault();
                updatePost()
            }}/>
        </div>
    )
}

export default Tools