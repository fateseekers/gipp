import React, {Component} from 'react'
import {DateConverter} from "../functions/news";
import {Color} from "../functions/img";
import {MDBIcon} from "mdbreact";
import {connect} from "react-redux";
import {errorHandle} from "../functions/errors";
import {successToast} from "../functions/toasts";
import {fetchWithAuth} from "../functions/fetchWithAuth";

class CommentsList extends Component {
    constructor(props) {
        super(props);

        this.deleteComment = this.deleteComment.bind(this)
    }

    deleteComment(e){
        e.preventDefault()
        fetchWithAuth(
            process.env.HOST_API + '/comments',
            {
                id: this.props.comment.id
            },
            "DELETE",
            this.props.token
        ).then(async (res) => {
            let result = await res.json()
            if(res.status !== 200){
                errorHandle(result.message)
            } else {
                successToast("Комментарий удален!")
            }
        })
    }

    render() {
        return (
            <div className="card-body card mb-3 mt-3">
                {this.props.user && (this.props.user.id === this.props.comment.author_id) || this.props.user && this.props.user.role === "Admin"
                    ?  <div className="newscard__tools">
                        <MDBIcon
                            className="mr-2 black-text"
                            icon="times-circle" size="2x" onClick={(e) => {
                            this.deleteComment(e)
                        }}/>
                    </div>
                    : null
                }

                <h5>{this.props.comment.role === "Publisher" ? `Издатель - ${this.props.comment.name}` : `Пользователь - ${this.props.comment.name}`}</h5>
                <p>{this.props.comment.text}</p>
                <p>{DateConverter(this.props.comment.date)}</p>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
    token: state.token,
});

export default connect(mapStateToProps, null)(CommentsList)
