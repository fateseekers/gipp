import React, {Component} from 'react'
import {DateConverter, StatusConverter} from "../functions/news";
import { Color } from "../functions/img";
import trimCharacters from 'trim-characters';
import Link from "next/link";
import {connect} from "react-redux";
import Tools from "./Tools";
import { toast } from "react-toastify";
import ModalForToast from "./Modal";
import Router from 'next/router'

class NewsCard extends Component{
    constructor(props) {
        super(props);

        this.state = {
            image: {
                rgb: "",
                url: ""
            },
            description: ""
        }

        this.deletePost = this.deletePost.bind(this)
        this.updatePost = this.updatePost.bind(this)
    }

    UNSAFE_componentWillMount() {
        let content = JSON.parse(this.props.post.content)

        content.find((el) => {
            if (el.type === 'image') {
                this.setState({
                    image: {
                        url: el.data.file.url,
                        rgb: el.data.file.rgb
                    }
                })
                return true
            }
        })
        content.find((el) => {
            if (el.type === "paragraph") {
                let description = trimCharacters(el.data.text, 55, true, '...');
                this.setState({description: description})
                return true
            }
        })
    }

    deletePost(id){
        toast(<ModalForToast id={id} />, {
            position: "top-right",
            autoClose: false,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: false,
            draggable: false,
            progress: undefined,
            limit: 1,
            closeButton: false
        })
    }

    updatePost(post){
        Router.push({
            pathname: '/editor',
            query: {
                edit: true,
                id: JSON.stringify(post.id)
            }
        }, undefined)
    }

    render() {
        return (
            <Link href={{pathname: '/news/' + this.props.post.id, query: {id: this.props.post.id}}}
                  as={`/news/${this.props.post.id}`}>
                <a>
                    <div className={`newscard${this.props.main ? " small" : ""}`}>
                        {this.props.user && (this.props.user.id === this.props.post.author_id && this.props.user.role === "Publisher" || this.props.user.role === "Admin") ? <Tools user_id={this.props.user.id} post_author_id={this.props.post.author_id} image={this.state.image.rgb} deletePost={() => this.deletePost(this.props.post.id)} updatePost={() => this.updatePost(this.props.post)}/> : null}
                        <div className="newscard__image--block">
                            <img className="newscard__img" src={this.state.image.url}/>
                            <div className="item__gradient"
                                 style={{backgroundImage: `linear-gradient(rgba(${this.state.image.rgb}, 0) 0%, rgba(${this.state.image.rgb}, 75) 100%)`}}/>
                        </div>
                        <div className="newscard__info d-flex flex-column"
                             style={{color: Color(this.state.image.rgb) ? "white" : "black"}}>
                            <div className="col-12">
                                <h4 className="newscard__heading h5"><strong>{trimCharacters(this.props.post.title, 35, true, '...')}</strong></h4>
                                {this.state.description && !this.props.main ?
                                    <p className="newscard__description">{this.state.description}</p> : null}
                            </div>
                            <div className="d-flex align-items-end justify-content-between col-12">
                                {!this.props.main ?
                                    <p className="newscard__theme">Рубрика: {StatusConverter(this.props.post.status)}</p> : null}
                                <p className="newscard__date">{DateConverter(this.props.post.date)}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </Link>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps, null)(NewsCard)
