import React, {Component} from 'react'
import {connect} from "react-redux";
import Router from 'next/router'
import {errorToast} from "../functions/toasts";
import {ToastContainer} from "react-toastify";
import Stub from "./Stub";

class Wrapper extends Component{
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }
    }

    componentDidMount() {
        if(this.props.user && (this.props.user.role === "Admin" || (this.props.user.role === "Publisher" && this.props.user.status === "Active"))) {
            this.setState({isLoaded: true})
        } else if (this.props.user && this.props.user.role === "Publisher" && this.props.user.status === "Moderate") {
            errorToast("Ваш аккаунт находится на модерации!")
            this.setState({isLoaded: "Moderate"})
        } else if (this.props.user && this.props.user.role === "User"){
            Router.push('/404')
        } else {
            Router.push('/auth/signin')
        }
    }

    render() {
        if(this.state.isLoaded === true){
            return this.props.children
        } else if(this.state.isLoaded === "Moderate") {
            return  (
                <>
                    <Stub text={"Ваш аккаунт на модерации, дождитесь проверки администратора!"}/>
                    <ToastContainer />
                </>
            )
        } else {
            return <p>Loading...</p>
        }
    }
}

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps, null)(Wrapper)