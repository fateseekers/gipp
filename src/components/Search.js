import React, {Component} from 'react'
import {MDBCol, MDBFormInline, MDBIcon} from "mdbreact";
import Router from "next/router";

class Search extends Component{
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }



    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()

        Router.push({
            pathname: '/search',
            query: {
                str: this.state.str,
                page: 0,
            }
        })
    }

    render() {
        return (
            <MDBFormInline waves={true} className="w-100" onSubmit={(e) => this.onSubmit(e)}>
                <div className="md-form my-0 w-100">
                    <input className="form-control mr-sm-2 w-100" autoComplete={"off"} aria-autocomplete="none" name="str" type="text" placeholder="Поиск" aria-label="Search" onChange={(e) => this.onChange(e)}/>
                </div>
            </MDBFormInline>
        );
    }
}

export default Search
