import React, {Component} from 'react'
import {connect} from "react-redux";
import {useRouter} from 'next/router'

const Logout = ({resetUser}) => {
    resetUser()
    const Router = useRouter()
    return Router.push('/')
}

const mapDispatchToProps = dispatch => {
    return {
        resetUser: () => dispatch({type: "RESETUSER"}),
    };
};


export default connect(null, mapDispatchToProps)(Logout)