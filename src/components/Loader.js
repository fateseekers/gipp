import React from 'react'

const Loader = () => {
    return (
        <div className="stub">
            <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Загрузка...</span>
            </div>
        </div>
    )
}

export default Loader