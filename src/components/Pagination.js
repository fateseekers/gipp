import React from 'react'
import Link from "next/link";

const Pagination = ({count, page, url}) => {
    return(
        <nav aria-label="Page navigation example">
            <ul className="pagination pg-indigo justify-content-center">
                {/*Предыдущая*/}
                {page > 1
                    ? <>
                        <li className={`page-item${count < (page * 10) ? " d-none" : ""}`}>
                            <Link href={url + (+page - 1)}>
                                <a className="page-link" tabIndex="-1">Предыдущая</a>
                            </Link>
                        </li>
                        <li className="page-item">
                            <Link href={url + (+page - 1)}>
                                <a className="page-link">{+page - 1}</a>
                            </Link>
                        </li>
                    </>
                    : null
                }
                {/*Текущая страница*/}
                <li className="page-item active">
                    <Link href={url + (+page)}>
                        <a className="page-link indigo">{+page + 1}</a>
                    </Link>
                </li>
                {/*Следующая*/}
                {(page * 10) > count
                    ? <>
                        <li className="page-item">
                            <Link href={url + (+page + 1)}>
                                <a className="page-link">{+page + 1}</a>
                            </Link>
                        </li>
                        <li className="page-item" aria-disabled={true}>
                            <Link href={url + (+page + 1)}>
                                <a className="page-link" aria-disabled={true}>Следующая</a>
                            </Link>
                        </li>
                    </>
                    : null
                }
            </ul>
        </nav>
    )
}

export default Pagination