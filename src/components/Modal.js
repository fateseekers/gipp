import React, { Component } from 'react';
import { MDBBtn } from 'mdbreact';
import { toast } from "react-toastify";
import {errorHandle} from "../functions/errors";
import {successToast} from "../functions/toasts";
import {connect} from "react-redux";

class ModalForToast extends Component {
    constructor(props) {
        super(props);
    }

    onDelete(id){
        if(!this.props.good) {
            fetch(process.env.HOST_API + '/posts', {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${this.props.token}`
                },
                body: JSON.stringify({id: id})
            }).then(async (res) => {
                let result = await res.json()
                if (res.status > 250) {
                    errorHandle(result.message)
                } else {
                    toast.dismiss()
                    successToast("Запись удалена")
                }
            })
        } else {
            fetch(process.env.HOST_API + '/catalog', {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${this.props.token}`
                },
                body: JSON.stringify({id: id})
            }).then(async (res) => {
                let result = await res.json()
                if (res.status > 250) {
                    errorHandle(result.message)
                } else {
                    toast.dismiss()
                    successToast("Товар удален")
                }
            })
        }
    }

    render() {
        return (
            <div className="d-flex flex-column px-2 py-1">
                <p>Вы действительно хотите удалить {this.props.good ? "данный товар" : "данную запись"}?</p>
                <div className="d-flex">
                    <MDBBtn color="success" onClick={() => this.onDelete(this.props.id)}>Да</MDBBtn>
                    <MDBBtn color="danger" onClick={() => toast.dismiss()}>Нет</MDBBtn>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    token: state.token
});

export default connect(mapStateToProps, null)(ModalForToast)