import React, {Component} from 'react'
import {MDBIcon, MDBTable, MDBTableBody, MDBTableHead} from "mdbreact";
import {connect} from "react-redux";
import {fetchWithAuth} from "../functions/fetchWithAuth";
import {errorHandle} from "../functions/errors";
import {successToast} from "../functions/toasts";

class ModerateTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: []
        }

        this.approveUser = this.approveUser.bind(this)
        this.rejectUser = this.rejectUser.bind(this)
    }

    componentDidMount() {
        fetchWithAuth(
            process.env.HOST_API + '/users',
            "",
            "GET",
            this.props.token

        ).then( async (res) => {
            let result = await res.json()
            this.setState({users: result.users})
        })
    }

    approveUser(id) {
        fetchWithAuth(
            process.env.HOST_API + '/usersstatus',
            {id: id, status: "Active"},
            "PUT",
            this.props.token
        ).then( async (res) => {
            let result = await res.json()
            if(res.status > 250){
                errorHandle(result.message)
            } else {
                let newArr = this.state.users.filter(function(user) {
                    return user.id !== id
                })
                this.setState({users: newArr})
                successToast(`Издатель одобрен!`)
            }
        })
    }


    rejectUser(id) {
        fetchWithAuth(
            process.env.HOST_API + '/users',
            {id: id},
            "DELETE",
            this.props.token
        ).then( async (res) => {
            let result = await res.json()
            if(res.status > 250){
                errorHandle(result.message)
            } else {
                let newArr = this.state.users.filter(function(user) {
                    return user.id !== id
                })
                this.setState({users: newArr})
                successToast(`Издатель отклонен!`)
            }
        })
    }

    render() {
        return (
            <div>
                <MDBTable className="moderating__table" responsive hover>
                    <caption>Аккаунты на модерации</caption>
                    <MDBTableHead className="w-100">
                        <tr>
                            <th scope="col">Издательство</th>
                            <th scope="col">Описание</th>
                            <th scope="col">Email</th>
                            <th scope="col">Одобрить</th>
                            <th scope="col">Отклонить</th>
                        </tr>
                    </MDBTableHead>
                    <MDBTableBody>
                        {this.state.users.map((user) => {
                            return <tr key={user.id}>
                                <td>{user.name}</td>
                                <td>{user.description}</td>
                                <td>{user.email}</td>
                                <td>
                                    <MDBIcon size="2x" icon="check" onClick={() => this.approveUser(user.id)}/>
                                </td>
                                <td>
                                    <MDBIcon size="2x" icon="times" onClick={() => this.rejectUser(user.id)}/>
                                </td>
                            </tr>
                        })}
                    </MDBTableBody>
                </MDBTable>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    token: state.token
});

export default connect(mapStateToProps, null)(ModerateTable)
