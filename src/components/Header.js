import React, {Component} from 'react'
import Navigation from "./Navigation";

class Header extends Component{
    render() {
        return (
            <header className="mb-4">
                <Navigation />
            </header>
        );
    }
}

export default Header