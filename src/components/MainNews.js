import React, {Component} from 'react'
import NewsCard from "./NewsCard";

class MainNews extends Component{
    render() {
        return (
            <>
                <h5>Главные новости:</h5>
                {this.props.posts.map((post) => {
                    return <NewsCard key={post.id} post={post} main={true}/>
                })}
            </>
        );
    }
}

export default MainNews