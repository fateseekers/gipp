import React, {Component} from 'react'
import {MDBBtn, MDBCol, MDBIcon, MDBInput} from "mdbreact";
import {errorHandle} from "../functions/errors";
import {errorToast, successToast} from "../functions/toasts";
import {connect} from "react-redux";

class ChangePassword extends Component{
    constructor(props) {
        super(props);


        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()

        if(this.state.newpassword !== this.state.confirmpassword){
            errorToast("Новые пароли не совпадают")
            return
        }

        if(this.state.oldpassword === this.state.newpassword){
            errorToast("Новый пароль совпадает со старым")
            return;
        }

        fetch(process.env.HOST_API + '/users', {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.props.token}`
            },
            body: JSON.stringify({
                id: this.props.user.id,
                oldpassword: this.state.oldpassword,
                newpassword: this.state.newpassword
            })
        }).then( async (res) => {
            let result = await res.json()
            if(res.status > 200){
                errorHandle(result.message)
            } else {
                successToast("Пароль успешно изменен!");
            }
        })
    }

    render() {
        return (
            <MDBCol md="6" className="mt-4 mx-auto">
                <form onSubmit={(e) => this.onSubmit(e)}>
                    <p className="h5 text-center mb-4">Смена пароля</p>
                    <div className="grey-text">
                        <MDBInput label="Старый пароль" icon="lock-open" name="oldpassword" group type="password" validate error="wrong"
                                  success="right" onChange={(e) => this.onChange(e)} required={true}/>
                        <MDBInput label="Новый пароль" icon="key" name="newpassword" group type="password" validate error="wrong"
                                  success="right"  onChange={(e) => this.onChange(e)} required={true}/>
                        <MDBInput label="Повторите новый пароль" icon="key" name="confirmpassword" group type="password" validate error="wrong"
                                  success="right"  onChange={(e) => this.onChange(e)} required={true}/>
                    </div>
                    <div className="text-center">
                        <MDBBtn outline color="indigo" type="submit">
                            Изменить
                            <MDBIcon far icon="paper-plane" className="ml-1" />
                        </MDBBtn>
                    </div>
                </form>
            </MDBCol>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
    token: state.token
});

// const mapDispatchToProps = dispatch => {
//     return {
//         resetUser: () => dispatch({type: "RESETUSER"}),
//     };
// };


export default connect(mapStateToProps, null)(ChangePassword)
