import React, {Component} from 'react'
import {MDBBtn} from "mdbreact";

class Filters extends Component{
    constructor(props) {
        super(props);

        this.state = {
            age: 0
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()
        this.props.getfilter(this.state.age, this.state.type)
    }

    render() {
        return (
            <>
                <h5>Фильтры:</h5>
                <div>
                    <form className="form d-flex flex-column align-items-center z-depth-1 p-2 p-sm-4 p-lg-2" onSubmit={(e) => this.onSubmit(e)}>
                        <div className="d-flex flex-column my-4">
                            <label htmlFor="range">Возрастное огр.:</label>
                            <div className="d-flex mb-2">
                                <div className="w-75">
                                    <input type="range" name="age" className="custom-range" id="range" min="0" max="16" step="1" value={this.state.age} onChange={(e) => this.onChange(e)}/>
                                </div>
                                <span className="font-weight-bold text-primary ml-5 valueSpan2 ml-lg-2">{this.state.age}</span>
                            </div>
                            <label htmlFor="select">Вид издания:</label>
                            <select id="select" className="mt-0 browser-default custom-select md-form" defaultValue="none" name="type" onChange={(e) => this.onChange(e)}>
                                <option value="none" disabled>Вид издания</option>
                                <option value="magazine">Газета</option>
                                <option value="journal">Журнал</option>
                            </select>
                            <MDBBtn color="indigo" type="submit">Поиск</MDBBtn>
                        </div>
                    </form>
                </div>
            </>
        );
    }
}

export default Filters
