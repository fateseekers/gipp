import React, {Component} from 'react'
import {
    FacebookIcon,
    FacebookShareButton,
    TelegramIcon, TelegramShareButton,
    VKIcon,
    VKShareButton,
    WhatsappIcon,
    WhatsappShareButton
} from "react-share";
import {MDBBtn, MDBIcon} from "mdbreact";
import {fetchWithAuth} from "../functions/fetchWithAuth";
import {connect} from "react-redux";
import {successToast} from "../functions/toasts";
import {errorHandle} from "../functions/errors";

class SocialShare extends Component {
    constructor(props) {
        super(props);

        this.state = {
            liked: false
        }

        this.giveLike = this.giveLike.bind(this)
    }

    giveLike(){
        fetchWithAuth(process.env.HOST_API + '/like',
            {id: this.props.id},
            "POST",
            this.props.token
        ).then(async (res) => {
            let result = await res.json()
            if(result.message === "LIKED"){
                this.setState({liked: true})
                successToast("Вам поставили отметку 'нравится'!")
            } else if(result.message === "DISLIKED") {
                this.setState({liked: false})
                successToast("Вам сняли отметку 'нравится'!")
            } else {
                errorHandle(result.message)
            }
        })
    }

    render() {
        return (
            <div className="d-flex flex-wrap justify-content-center">
                <FacebookShareButton
                    url={this.props.url}
                    quote={this.props.title}
                    className="Demo__some-network__share-button btn rounded-circle"
                >
                    <FacebookIcon size={48} round />
                </FacebookShareButton>
                <VKShareButton
                    url={this.props.url}
                    quote={this.props.title}
                    className="Demo__some-network__share-button btn rounded-circle"
                >
                    <VKIcon size={48} round />
                </VKShareButton>
                <TelegramShareButton
                    url={this.props.url}
                    quote={this.props.title}
                    className="Demo__some-network__share-button btn rounded-circle"
                >
                    <TelegramIcon size={48} round />
                </TelegramShareButton>
                <WhatsappShareButton
                    url={this.props.url}
                    quote={this.props.title}
                    className="Demo__some-network__share-button btn rounded-circle"
                >
                    <WhatsappIcon size={48} round />
                </WhatsappShareButton>
                {this.props.like
                ? <div className="d-flex align-items-center justify-content-center">
                        <MDBBtn color="none" className="rounded-circle p-0 d-flex align-items-center justify-content-center px-2" style={{height: "48px", width: "48px"}} onClick={() => {this.giveLike()}}>
                            <MDBIcon icon="heart" size="2x" className={this.state.liked ? "red-text" : "grey-text"}/>
                        </MDBBtn>
                        <p className="mb-0">{this.props.likes}+</p>
                    </div>
                : null
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    token: state.token
});

export default connect(mapStateToProps, null)(SocialShare)
