import React from 'react'
import Link from 'next/link'

const Stub = ({text, err}) => {
    return(
        <div className="stub">
            <h3>{text} {err === 404 ? <Link href='/'><a>Вернитесь на главную страницу.</a></Link> : ""}</h3>
            <img className="stub__img" src="/person.svg" alt=""/>
        </div>
    )
}

export default Stub