import React, {Component} from 'react';
import {
    MDBBtn,
    MDBCollapse,
    MDBFormInline,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarNav,
    MDBNavbarToggler,
    MDBNavItem
} from "mdbreact";
import Link from "next/link";
import {connect} from "react-redux";
import {successToast} from "../functions/toasts";
import Search from "./Search";

class Navigation extends Component{
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };
    }

    toggleCollapse = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }

    logout(){
        this.props.resetUser()
        successToast("Вы вышли из системы!");
    }

    render() {
        return (
            <MDBNavbar color="indigo" dark expand="md">
                <MDBNavbarBrand>
                    <strong className="white-text">
                        <Link href="/">
                            <a className="white-text">АДМПП</a>
                        </Link>
                    </strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler onClick={this.toggleCollapse} />
                <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
                    <MDBNavbarNav left className="d-flex justify-content-between">
                        <MDBNavItem>
                            <MDBBtn color="indigo" waves={"true"} className="d-flex justify-content-center w-100 w-md-auto btn-sm p-md-2">
                                <Link href="/news?page=0console.">
                                    <a className="text-white">Новости</a>
                                </Link>
                            </MDBBtn>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBBtn color="indigo" waves={"true"} className="d-flex justify-content-center w-100 w-md-auto btn-sm p-md-2">
                                <Link href="/publishers">
                                    <a className="text-white">Издатели</a>
                                </Link>
                            </MDBBtn>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBBtn color="indigo" waves={"true"} className="d-flex justify-content-center w-100 w-md-auto btn-sm p-md-2">
                                <Link href="/catalog?page=0">
                                    <a className="text-white">Каталог</a>
                                </Link>
                            </MDBBtn>
                        </MDBNavItem>
                    </MDBNavbarNav>
                    <MDBNavbarNav right>
                        <MDBNavItem>
                            <Search />
                        </MDBNavItem>
                        {this.props.user
                            ?
                            <>
                                {this.props.user.role === "Admin" || this.props.user.role === "Publisher"
                                    ?
                                    <MDBNavItem>
                                        <MDBBtn color="indigo" waves={"true"} className="d-flex justify-content-center w-100 w-md-auto btn-sm p-md-2">
                                            <Link href="/cabinet">
                                                <a className="text-white">Кабинет</a>
                                            </Link>
                                        </MDBBtn>
                                    </MDBNavItem>
                                    :
                                    null
                                }
                                <MDBNavItem>
                                    <MDBBtn color="indigo" waves={"true"} className="d-flex justify-content-center w-100 w-md-auto btn-sm p-md-2 mr-md-4" onClick={() => this.logout()}>
                                        <Link href="/">
                                            <a className="text-white">Выход</a>
                                        </Link>
                                    </MDBBtn>
                                </MDBNavItem>
                            </>
                            : <MDBNavItem>
                                <MDBBtn color="indigo" waves={"true"} className="d-flex justify-content-center w-100 w-md-auto btn-sm p-md-2">
                                    <Link href="/auth/signup">
                                        <a className="text-white">Регистрация</a>
                                    </Link>
                                </MDBBtn>
                            </MDBNavItem>
                        }

                    </MDBNavbarNav>
                </MDBCollapse>
            </MDBNavbar>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        resetUser: () => dispatch({type: "RESETUSER"}),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Navigation)
