import React, {Component} from "react";
import { MDBCol, MDBBtn, MDBIcon, MDBInput } from 'mdbreact';

class ContactUs extends Component{
    render() {
        return (
            <MDBCol md="6" className="mx-auto">
                <form>
                    <p className="h5 text-center mt-4">Свяжитесь с нами</p>
                    <div className="grey-text">
                        <MDBInput label="Ваше имя" icon="user" group type="text" validate error="wrong"
                                  success="right" />
                        <MDBInput label="Ваш email" icon="envelope" group type="email" validate error="wrong"
                                  success="right" />
                        <MDBInput label="Тема" icon="tag" group type="text" validate error="wrong" success="right" />
                        <MDBInput type="textarea" rows="2" label="Ваш вопрос" icon="pencil-alt" />
                    </div>
                    <div className="text-center">
                        <MDBBtn outline color="indigo">
                            Отправить
                            <MDBIcon far icon="paper-plane" className="ml-1" />
                        </MDBBtn>
                    </div>
                </form>
            </MDBCol>
        )
    }
};

export default ContactUs;