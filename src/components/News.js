import React, {Component} from 'react'
import NewsCard from "./NewsCard";

class News extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <h5>Новости:</h5>
                {this.props.posts.map((post) => {
                    return <NewsCard key={post.id} post={post} main={false}/>
                })}
            </>
        );
    }
}

export default News