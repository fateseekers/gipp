import React, {Component} from 'react'
import {DateConverter, StatusConverter} from "../functions/news";
import Link from "next/link";
import Tools from "./Tools";
import Router from "next/router";
import {connect} from "react-redux";
import {toast} from "react-toastify";
import ModalForToast from "./Modal";

class GoodsCard extends Component{

    deleteGood(id){
        toast(<ModalForToast id={id} good={true}/>, {
            position: "top-right",
            autoClose: false,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: false,
            draggable: false,
            progress: undefined,
            limit: 1,
            closeButton: false
        })
    }

    updateGood(good){
        Router.push({
            pathname: '/catalog/add',
            query: {
                edit: true,
                id: JSON.stringify(good.id)
            }
        }, undefined)
    }

    render() {
        return (
            <Link href={'/catalog/' + this.props.good.id}>
                <a className={"black-text"}>
                    <div className="d-flex flex-column z-depth-1 p-3 mb-4 position-relative goodscard">
                        {this.props.user && (this.props.user.id === this.props.good.author_id && this.props.user.role === "Publisher" || this.props.user.role === "Admin") ? <Tools deletePost={() => this.deleteGood(this.props.good.id)} updatePost={() => this.updateGood(this.props.good)}/> : null}
                        <h5>Издание: {this.props.good.name}</h5>
                        <hr/>
                        <p>Вид: {StatusConverter(this.props.good.type)}</p>
                        <p>Дата: {DateConverter(this.props.good.date)}</p>
                        <p>Территория: {this.props.good.place}</p>
                        <p>Возрастное ограничение: {this.props.good.age}+</p>
                        <h5>Издатель: {this.props.good.publisher_name}</h5>
                    </div>
                </a>
            </Link>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps, null)(GoodsCard)
