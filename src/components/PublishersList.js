import React, {Component} from 'react'
import Link from "next/link";

class PublishersList extends Component{
    render() {
        return (
            <div className="col-12 col-md-6 mb-4 mb-md-auto">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Издатель: {this.props.publisher.name}</h5>
                        <p className="card-text">{this.props.publisher.description || ""}</p>
                        <Link href={"/news/author/" + this.props.publisher.id + "?page=0"}>
                            <a className="card-link mb-auto">Просмотреть записи</a>
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default PublishersList