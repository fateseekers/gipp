import React, {Component} from 'react'
import {MDBBtn, MDBInput} from "mdbreact";
import fetch from "isomorphic-unfetch";
import CommentsList from "./CommentsList";
import {connect} from "react-redux";
import {fetchWithAuth} from "../functions/fetchWithAuth";
import {errorHandle} from "../functions/errors";
import {successToast} from "../functions/toasts";

class Comments extends Component{
    constructor(props) {
        super(props);

        this.state = {
            comments: []
        }

        this.onChange = this.onChange.bind(this)
        this.sendComment = this.sendComment.bind(this)
    }

    componentWillMount() {
        fetch(process.env.HOST_API + `/comments_by_post_id`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json"
            },
            body: JSON.stringify({id: this.props.id})
        }).then(async (res) => {
            let json = await res.json()
            this.setState({comments: json.comments})
        })
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    sendComment(e){
        e.preventDefault()
        fetchWithAuth(
            process.env.HOST_API + '/comments',
            {
                post_id: this.props.id,
                text: this.state.comment
            },
            "POST",
            this.props.token
        ).then(async (res) => {
            let result = await res.json()
            if(res.status > 250){
                errorHandle(result.message)
            } else {
                successToast("Вы добавили комментарий!")
            }
        })
    }

    render() {
        return (
            <>
                {this.props.user
                    ?
                    <>
                        <div className="d-flex flex-column mb-4">
                            <form className="d-flex flex-column flex-md-row" onSubmit={(e) => this.sendComment(e)}>
                                <div className="col-12 d-flex align-items-center col-md-7 col-lg-9">
                                    <MDBInput className="lg-textarea" type="textarea" name="comment" label="Введите комментарий" rows="2" required={true} onChange={(e) => this.onChange(e)}/>
                                </div>
                                <div className="col-12 d-flex align-items-center justify-content-center col-md-5 col-lg-3">
                                    <MDBBtn className="w-100" color="success" type="submit">Отправить</MDBBtn>
                                </div>
                            </form>
                        </div>
                        <hr/>
                    </>
                    : null
                }
                <div className="d-flex flex-column mb-4">
                    <div className="col-12">
                        <h5>Комментарии:</h5>
                        {this.state.comments.length !== 0  ? this.state.comments.map((comment) => {
                            return <CommentsList key={comment.id} comment={comment} />
                        }) : <div>
                            <h5>Комментарии отсутствуют</h5>
                        </div>}
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
    token: state.token
});

export default connect(mapStateToProps, null)(Comments)
