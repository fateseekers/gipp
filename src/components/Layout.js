import React, {Component} from 'react'
import Head from 'next/head'
import Header from "./Header";
import Meta from "./Meta";
import Footer from "./Footer";
import {ToastContainer} from "react-toastify";
import Modal from "./Modal";

class Layout extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <Meta title={this.props.title || ""} description={this.props.description || ""} keywords={this.props.keywords || ""}/>
                <Header />
                {this.props.children}
                <Footer/>
                <ToastContainer limit={1}/>
            </>
        );
    }
}

export default Layout